package main

import (
	"net/http"
	"rest-api/config"
	"rest-api/internal/contoller"
	"rest-api/internal/repository"
	"rest-api/internal/router"
	"rest-api/internal/service"
	"rest-api/pkg/helper"
	
	"github.com/rs/zerolog/log"
)

func main() {
	log.Info().Msg("Started Server!")

	db := config.DatabaseConn()
	
	repository := repository.NewBookRepository(db)
	
	service := service.NewBookServiceImpl(repository)

	controller := contoller.NewBookController(service)

	routes := router.NewRouter(controller)

	server := &http.Server{
		Addr: ":8080",
		Handler: routes,
	}

	err := server.ListenAndServe()
	helper.ErrorPanic(err)
}