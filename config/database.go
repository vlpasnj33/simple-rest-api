package config

import (
	"database/sql"
	"fmt"
	"rest-api/pkg/helper"

	_ "github.com/lib/pq"
	"github.com/rs/zerolog/log"
)

const (
	host     = "localhost"
	port     = 5432
	user     = "postgres"
	password = "vova1697"
	dbName   = "books"
)

func DatabaseConn() *sql.DB {
	sqlInfo := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable", 
							host, port, user, password, dbName)
	
	db, err := sql.Open("postgres", sqlInfo)
	helper.ErrorPanic(err)

	err = db.Ping()
	helper.ErrorPanic(err)

	log.Info().Msg("Connected to database!!")

	return db
}