package router

import (
	"net/http"
	"rest-api/internal/contoller"

	"github.com/gin-gonic/gin"
)

func NewRouter(bookController *contoller.BookController) *gin.Engine {
	router := gin.Default()

	router.GET("", func(ctx *gin.Context) {
		ctx.JSON(http.StatusOK, "Home Page")
	})

	baseRouter := router.Group("/api")
	booksRouter := baseRouter.Group("/books")
	booksRouter.GET("", bookController.FindAll)
	booksRouter.GET("/:bookId", bookController.FindById)
	booksRouter.POST("", bookController.Create)
	booksRouter.PATCH("/:bookId", bookController.Update)
	booksRouter.DELETE("/:bookId", bookController.Delete)

	return router
}