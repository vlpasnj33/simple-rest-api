package contoller

import (
	"net/http"
	"rest-api/internal/service"
	"rest-api/internal/data"
	"rest-api/pkg/helper"
	"strconv"

	"github.com/gin-gonic/gin"
)

type BookController struct {
	BookService service.BookService
}

func NewBookController(bookService service.BookService) *BookController {
	return &BookController{BookService: bookService}
}

func(b *BookController) Create(ctx *gin.Context) {
	createRequest := data.BookCreateRequest{}
	err := ctx.ShouldBindJSON(&createRequest)
	helper.ErrorPanic(err)

	b.BookService.Create(createRequest)
	response := data.WebResponse{
		Code: http.StatusOK,
		Status: "OK",
		Data: nil,
	}
	ctx.Header("Content-Type", "application/json")
	ctx.JSON(http.StatusOK, response)
}

func(b *BookController) Update(ctx *gin.Context) {
	updateRequest := data.BookUpdateRequest{}
	err := ctx.ShouldBindJSON(&updateRequest)
	helper.ErrorPanic(err)

	bookId := ctx.Param("bookId")
	id, err := strconv.Atoi(bookId)
	helper.ErrorPanic(err)

	updateRequest.Id = id
	b.BookService.Update(updateRequest)

	response := data.WebResponse{
		Code: http.StatusOK,
		Status: "OK",
		Data: nil,
	}
	ctx.Header("Content-Type", "application/json")
	ctx.JSON(http.StatusOK, response)
}

func(b *BookController) Delete(ctx *gin.Context) {
	bookId := ctx.Param("bookId")
	id, err := strconv.Atoi(bookId)
	helper.ErrorPanic(err)

	b.BookService.Delete(id)

	response := data.WebResponse{
		Code: http.StatusOK,
		Status: "OK",
		Data: nil,
	}
	ctx.Header("Content-Type", "application/json")
	ctx.JSON(http.StatusOK, response)	
}

func(b *BookController) FindById(ctx *gin.Context) {
	bookId := ctx.Param("bookId")
	id, err := strconv.Atoi(bookId)
	helper.ErrorPanic(err)

	book := b.BookService.FindById(id)

	response := data.WebResponse{
		Code: http.StatusOK,
		Status: "OK",
		Data: book,
	}
	ctx.Header("Content-Type", "application/json")
	ctx.JSON(http.StatusOK, response)
}

func(b *BookController) FindAll(ctx *gin.Context) {
	result := b.BookService.FindAll()

	response := data.WebResponse{
		Code: http.StatusOK,
		Status: "OK",
		Data: result,
	}
	ctx.Header("Content-Type", "application/json")
	ctx.JSON(http.StatusOK, response)
}