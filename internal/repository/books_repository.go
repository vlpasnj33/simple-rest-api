package repository

import (
	"database/sql"
	"errors"
	"rest-api/internal/model"
	"rest-api/pkg/helper"
)

type BookRepository interface {
	Save(book model.Book)
	Update(book model.Book)
	Delete(bookId int)
	FindById(bookId int) (book model.Book, err error)
	FindAll() []model.Book
}

type BookRepositoryImpl struct {
	dataBase *sql.DB
}

func NewBookRepository(db *sql.DB) BookRepository {
	return &BookRepositoryImpl{dataBase: db}
}

func (b *BookRepositoryImpl) Save(book model.Book) {
	tx, err := b.dataBase.Begin()
	helper.ErrorPanic(err)
	defer helper.CommitOrRollback(tx)

	SQL := "INSERT INTO books(name) values ($1)"
	_, err = tx.Exec(SQL, book.Name)
	helper.ErrorPanic(err)
}

func (b *BookRepositoryImpl) Update(book model.Book) {
	tx, err := b.dataBase.Begin()
	helper.ErrorPanic(err)
	defer helper.CommitOrRollback(tx)

	SQL := "UPDATE books SET name=$1 WHERE id=$2"
	_, err = tx.Exec(SQL, book.Name, book.Id)
	helper.ErrorPanic(err)
}

func (b *BookRepositoryImpl) Delete(bookId int) {
	tx, err := b.dataBase.Begin()
	helper.ErrorPanic(err)
	defer helper.CommitOrRollback(tx)

	SQL := "DELETE FROM books WHERE id =$1"
	_, err = tx.Exec(SQL, bookId)
	helper.ErrorPanic(err)
}

func (b *BookRepositoryImpl) FindById(bookId int) (model.Book, error) {
	tx, err := b.dataBase.Begin()
	helper.ErrorPanic(err)
	defer helper.CommitOrRollback(tx)

	SQL := "SELECT id, name FROM books WHERE id=$1"
	res, err := tx.Query(SQL, bookId)
	helper.ErrorPanic(err)
	defer res.Close()

	book := model.Book{}

	if res.Next() {
		err := res.Scan(&book.Id, &book.Name)
		helper.ErrorPanic(err)
		return book, nil
	} else {
		return book, errors.New("Not found")
	}
}

func (b *BookRepositoryImpl) FindAll() ([]model.Book) {
	tx, err := b.dataBase.Begin()
	helper.ErrorPanic(err)
	defer helper.CommitOrRollback(tx)

	SQL := "SELECT id, name FROM books"
	res, err := tx.Query(SQL)
	helper.ErrorPanic(err)
	defer res.Close()

	var books []model.Book

	for res.Next() {
		book := model.Book{}
		err := res.Scan(&book.Id, &book.Name)
		helper.ErrorPanic(err)
		books = append(books, book)
	}
	return books
}

