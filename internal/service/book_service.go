package service

import (
	"rest-api/internal/model"
	"rest-api/internal/repository"
	"rest-api/internal/data"
	"rest-api/pkg/helper"
)

type BookService interface {
	Create(request data.BookCreateRequest)
	Update(request data.BookUpdateRequest)
	Delete(bookId int)
	FindById(bookId int) data.BookResponse
	FindAll() []data.BookResponse
}

type BookServiceImpl struct {
	BookRepository repository.BookRepository
}

func NewBookServiceImpl(bookRepository repository.BookRepository) BookService {
	return &BookServiceImpl{BookRepository: bookRepository}
}

func (b *BookServiceImpl) Create(request data.BookCreateRequest) {
	book := model.Book{
		Name: request.Name,
	}
	b.BookRepository.Save(book)
}

func (b *BookServiceImpl) Update(request data.BookUpdateRequest) {
	book, err := b.BookRepository.FindById(request.Id)
	helper.ErrorPanic(err)

	book.Name =  request.Name
	b.BookRepository.Update(book)
}

func (b *BookServiceImpl) Delete(bookId int) {
	book, err := b.BookRepository.FindById(bookId)
	helper.ErrorPanic(err)
	b.BookRepository.Delete(book.Id)
}

func (b *BookServiceImpl) FindById(bookId int) data.BookResponse {
	book, err := b.BookRepository.FindById(bookId)
	helper.ErrorPanic(err)
	return data.BookResponse(book)
} 

func (b *BookServiceImpl) FindAll() []data.BookResponse {
	books := b.BookRepository.FindAll()

	var bookResp []data.BookResponse

	for _, value := range books {
		book := data.BookResponse{Id: value.Id, Name: value.Name}
		bookResp = append(bookResp, book)
	}

	return bookResp
}